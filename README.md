# CMT3
This is the working branch of a brand new CMT version.

## Goals
 * Better permission model, each cluster/environent has his own users/groups
 * Data in the database is protected by cluster/environment permission model
 * Remain backwards compatible with CMT 2 API and template language
 * CMT server in Django 2.2 and Python 3.7+
 * CMT client compatble with Python 2.7 and Python 3.4+

## Pipenv
From CMT3 we shall use the Pipenv method of creating the virtualenv.

Installation on Fedora:
```
dnf install pipenv
```

Installation on MacOS with homebrew:
```
brew install pipenv
```

Installation on any platform with pip:
```
pip --user install pipenv
```

To create a virtual environment, use `pipenv sync`. This
installs the exact package versions listed in Pipfile.lock.

Check the following websites for more information on how to use pipenv:
 - https://pipenv.readthedocs.io/en/latest/
 - https://realpython.com/pipenv-guide/
