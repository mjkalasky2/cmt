from django.contrib import admin

from cmdb.models import *


class VendorAdmin(admin.ModelAdmin):
    pass


class LocationAdmin(admin.ModelAdmin):
    pass


class EquipmentModelAdmin(admin.ModelAdmin):
    pass


class EquipmentRackAdmin(admin.ModelAdmin):
    pass


class SubnetInline(admin.TabularInline):
    model = Subnet
    extra = 1


class NetworkAdmin(admin.ModelAdmin):
    list_display = (
        'name', 'vlan', 'subnet_count'
    )
    ordering = ('vlan',)
    inlines = [
        SubnetInline
    ]


class SubnetAdmin(admin.ModelAdmin):
    list_display = (
        'name', 'is_public', 'network_name', 'cidr', 'ip_version'
    )


class InterfaceAdmin(admin.ModelAdmin):
    pass


class InterfaceInline(admin.TabularInline):
    model = Interface
    extra = 1


class EquipmentMetaInline(admin.TabularInline):
    model = EquipmentMeta
    extra = 1


class EquipmentAdmin(admin.ModelAdmin):
    inlines = [
        InterfaceInline, EquipmentMetaInline
    ]


# Register the hooks
#admin.site.register(Subnet, SubnetAdmin)
admin.site.register(Network, NetworkAdmin)

admin.site.register(Vendor, VendorAdmin)
admin.site.register(Location, LocationAdmin)
admin.site.register(EquipmentModel, EquipmentModelAdmin)
admin.site.register(EquipmentRack, EquipmentRackAdmin)
admin.site.register(Interface, InterfaceAdmin)
admin.site.register(Equipment, EquipmentAdmin)
