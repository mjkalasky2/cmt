
from django.conf import settings
from django.db import models
from django.core.exceptions import ValidationError

from hashid_field import HashidAutoField

from IPy import IP

from cmt_base.tools import get_modules
from dns.models import Domain
from cmdb import fields


class Vendor(models.Model):
    id = HashidAutoField(primary_key=True)
    name = models.CharField(max_length=32)
    home_url = models.URLField()
    support_url = models.URLField()
    support_api = models.CharField(max_length=32, choices=get_modules())

    def __str__(self):
        return self.name


class Location(models.Model):
    id = HashidAutoField(primary_key=True)
    name = models.CharField(max_length=32)

    def __str__(self):
        return self.name


class EquipmentModel(models.Model):
    id = HashidAutoField(primary_key=True)
    name = models.CharField(max_length=32)
    vendor = models.ForeignKey(Vendor, on_delete=models.DO_NOTHING)

    def __str__(self):
        return self.name


class EquipmentRack(models.Model):
    id = HashidAutoField(primary_key=True)
    name = models.CharField(max_length=32)
    location = models.ForeignKey(Location, on_delete=models.DO_NOTHING)

    def __str__(self):
        return self.name


class Network(models.Model):
    id = HashidAutoField(primary_key=True)
    name = models.CharField(max_length=32)
    vlan = models.IntegerField()

    @property
    def subnet_count(self):
        return self.subnet_set.count()

    def __str__(self):
        return 'network-%s' % self.name


class Subnet(models.Model):
    id = HashidAutoField(primary_key=True)
    name = models.CharField(max_length=64, editable=False)
    is_public = models.BooleanField()
    cidr = fields.Cidr()
    gateway = models.CharField(max_length=32, blank=True, null=True)
    network = models.ForeignKey(Network, on_delete=models.CASCADE)

    def clean(self):

        errors = dict()

        # When a network is public check if it's unique within the whole database
        if self.is_public:
            try:
                result = self.__class__.objects.filter(cidr=self.cidr, is_public=True)
                if len(result) > 1 or (result and result[0].id != self.id):
                    errors['cidr'] = ValidationError(
                        'CIDR needs to be unique with the whole database, due to public network', code='invalid'
                    )
            except self.DoesNotExist:
                pass

        # We still need to check if a subnet is unique in the network!
        try:
            result = self.__class__.objects.filter(cidr=self.cidr, network=self.network)
            if len(result) > 1 or (result and result[0].id != self.id):
                errors['cidr'] = ValidationError(
                    'CIDR needs to be unique within the network %s' % self.network.name, code='invalid'
                )
        except self.DoesNotExist:
            pass

        # Make sure we have the gateway configured (and only do this at first save!)
        if not self.gateway and not self.id:
            if not isinstance(self.cidr, IP):
                try:
                    ip = IP(self.cidr)
                except ValueError:
                    errors['cidr'] = ValidationError(
                        'CIDR is invalid %s' % self.cidr, code='invalid'
                    )
            else:
                ip = self.cidr

            if not errors:
                self.gateway = ip[1].strNormal()

        if errors:
            raise ValidationError(errors)

    @property
    def ip_version(self):
        return 'IPv%d' % self.cidr.version()

    @property
    def network_name(self):
        return self.network.name

    def save(self, *args, **kwargs):
        self.name = '%s-%s' % (self.network.name, str(self.cidr))
        super().save(*args, **kwargs)

    def __str__(self):
        return self.name


class Equipment(models.Model):
    id = HashidAutoField(primary_key=True)
    skip_in_dns = models.BooleanField()
    label = models.CharField(max_length=32)
    warranty_tag = models.CharField(max_length=32)
    model = models.ForeignKey(EquipmentModel, on_delete=models.DO_NOTHING)
    rack = models.ForeignKey(EquipmentRack, on_delete=models.DO_NOTHING)
    slot = models.IntegerField()


class Interface(models.Model):
    id = HashidAutoField(primary_key=True)
    label = models.CharField(max_length=32)
    equipment = models.ForeignKey(Equipment, on_delete=models.DO_NOTHING)
    domain = models.ForeignKey(Domain, on_delete=models.DO_NOTHING)
    ip = models.GenericIPAddressField()
    aliases = models.CharField(max_length=32, null=True, blank=True)
    hwaddress = models.CharField(max_length=32, null=True, blank=True)
    subnet = models.ForeignKey(Subnet, on_delete=models.DO_NOTHING)
    type = models.CharField(max_length=32, choices=settings.CMDB_INTERFACE_TYPES)


class EquipmentMeta(models.Model):
    id = HashidAutoField(primary_key=True)
    equipment = models.ForeignKey(Equipment, on_delete=models.DO_NOTHING)
    key = models.CharField(max_length=32)
    value = models.TextField()

    def __str__(self):
        return self.name
